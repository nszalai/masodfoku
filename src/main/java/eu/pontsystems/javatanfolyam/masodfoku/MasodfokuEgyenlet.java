package eu.pontsystems.javatanfolyam.masodfoku;

/**
 * Created by nszalai on 2014.09.28..
 */
public class MasodfokuEgyenlet {
    private double a = 0;
    private double b = 0;
    private double c = 0;

    public MasodfokuEgyenlet(final double aParam, final double bParam, final double cParam) {
        a = aParam;
        b = bParam;
        c = cParam;
    }

    public Eredmeny kalkulal() {

        double d = getDiszkriminans();
        double elso = 0;
        double masodik = 0;
        String errorMsg = "";

        if (d < 0) {
            errorMsg = "Az egyenletnek nincs megoldasa a valos szamok halmazan.";
        } else {
            elso = ((-1) * b - Math.sqrt(d)) / (2 * a);
            masodik = ((-1) * b + Math.sqrt(d)) / (2 * a);
        }

        return new Eredmeny(elso, masodik, errorMsg);
    }

    private double getDiszkriminans() {
        return b * b - 4 * a * c;
    }

}
