package eu.pontsystems.javatanfolyam.masodfoku;

/**
 * Created by nszalai on 2014.09.28..
 */
public class Eredmeny {
    private String errorMsg = "";
    private Double x1;
    private Double x2;

    Eredmeny(final double x1Param, final double x2Param, final String msg) {
        x1 = x1Param;
        x2 = x2Param;
        errorMsg = msg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public String toString() {
        StringBuilder res = new StringBuilder("A masodfoku egyenlet megoldasa: \n");

        if (errorMsg == "" && !x1.isNaN() && !x2.isNaN()) {
            if (x1.compareTo(x2) != 0) {
                res.append("x1 = ");
                res.append(x1);
                res.append("; x2 = ");
                res.append(x2);
            } else {
                res.append("x = ");
                res.append(x1);
            }
        } else {
            res.append(errorMsg);
        }

        return res.toString();
    }

}
