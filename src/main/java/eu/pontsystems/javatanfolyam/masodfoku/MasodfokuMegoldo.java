// fordítani: javac *.java -d .
// futtatni: java tanfolyam.MasodfokuMegoldo

package eu.pontsystems.javatanfolyam.masodfoku;

/**
 * Created by nszalai on 2014.09.28..
 */
public class MasodfokuMegoldo {

    public static void main(String[] args) {
        MasodfokuEgyenlet masodfokuEgyenlet = new MasodfokuEgyenlet(5, 0, -80);
        System.out.println(masodfokuEgyenlet.kalkulal().toString());

        MasodfokuEgyenlet masodfokuEgyenlet2 = new MasodfokuEgyenlet(1, -22, 121);
        System.out.println(masodfokuEgyenlet2.kalkulal().toString());

    }

}
